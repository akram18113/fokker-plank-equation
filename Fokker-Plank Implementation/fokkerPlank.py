import numpy as np
from scipy.integrate import solve_ivp

# Define parameters
D = 1.0    # Diffusion coefficient
mu = 0.5   # Drift coefficient
xmin = -5  # Lower bound of position range
xmax = 5   # Upper bound of position range
tmin = 0   # Initial time
tmax = 10  # Final time
N = 100    # Number of grid points

# Define the grid
x = np.linspace(xmin, xmax, N)

# Define the initial condition
def init_cond(x):
    return np.exp(-x**2)

# Define the Fokker-Planck equation
def fokker_planck(t, y):
    dydt = np.zeros(N)
    dx = x[1] - x[0]
    dydt[0] = (D/dx**2)*(y[1]-y[0]) - mu/dx*(y[1]-y[0])
    for i in range(1, N-1):
        dydt[i] = (D/dx**2)*(y[i+1]-2*y[i]+y[i-1]) - mu/dx*(y[i+1]-y[i-1])
    dydt[N-1] = (D/dx**2)*(y[N-2]-y[N-1]) - mu/dx*(y[N-2]-y[N-1])
    return dydt

# Solve the Fokker-Planck equation
sol = solve_ivp(fokker_planck, [tmin, tmax], init_cond(x), t_eval=np.linspace(tmin, tmax, 101))

# Plot the solution
import matplotlib.pyplot as plt
for i in range(sol.y.shape[1]):
    plt.plot(x, sol.y[:,i], label='t={:.2f}'.format(sol.t[i]))
plt.xlabel('Position')
plt.ylabel('Probability density')
plt.legend()
plt.show()

